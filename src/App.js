import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import ReactNotification from 'react-notifications-component'

import Layout from './components/Layout/Layout'
import Landing from './containers/Landing/Landing'
import Products from './containers/Products/Products'
import Auth from './containers/Auth/Auth'
import AOS from 'aos';
import 'aos/dist/aos.css';
class App extends Component {
  componentDidMount() {
    AOS.init({
      duration: 2000
    })
  }
  render() {
    return (
      <div>
      <ReactNotification/>
        <Layout>
          <Switch>
          <Route path="/auth" component={Auth}/>
            <Route path='/products' component={Products} />
            <Route path='/' exact component={Landing} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
