import * as actionTypes from "./actionTypes"

export const seeProduct =(product)=>{
    return{
        type:actionTypes.SEE_PRODUCT,
        product:product
    }
}