import * as actionTypes from "./actionTypes"

export const addToCart =(cartItem)=>{
    return{
        type:actionTypes.ADD_CART,
        cartItem:cartItem
    }
}

export const deleteCartItem = (item)=>{
    return{
        type:actionTypes.DELETE_CART_ITEM,
        item:item
    }
}