import * as actionTypes from "./actionTypes"

export const orderProduct = (orderItem) => {
    return {
        type: actionTypes.ORDER_PRODUCT,
        orderItem:orderItem
    }
}