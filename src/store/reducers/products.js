import * as actionTypes from '../actions/actionTypes'
import { updatedObject } from '../Utility'

const initialState = {
    products: [
        { id: 1, name: 'Burger', price: 1200, recipe: "Lorem,Ipsum,dolor,Cadsum", Date: "" },
        { id: 2, name: 'Shawarma', price: 800, recipe: 'Dolor,bagsum,gorum,ipsum', Date: "" },
        { id: 3, name: 'Fried Chicken(Half)', price: 2000, recipe: 'lolor,lopot', Date: "" },
        { id: 4, name: "Yellow Yam(packets)", price: 200, recipe: 'Yam,opot', Date: "" },
        { id: 5, name: "Grilled Bread", price: 500, recipe: 'Yam,opot', Date: "" }
    ],
    product: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEE_PRODUCT:
            let update = updatedObject(state, { product: action.product })
            return update
        default:
            return state
    }
}

export default reducer