import * as actionTypes from '../actions/actionTypes'
import { updatedObject } from "../Utility"

const initialState = {
    orders: [],
    loading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ORDER_PRODUCT:
            let update = updatedObject(action.orderItem, { Date: new Date() })
            let orders = state.orders.concat(update)
            return {
                ...state,
                orders: orders
            }
        default:
            return state
    }

}

export default reducer