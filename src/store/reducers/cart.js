import * as actionTypes from '../actions/actionTypes'

const initialState = {
    cart: [],
    loading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_CART:
                let cart= state.cart.concat(action.cartItem)
                return {
                  ...state,
                  cart:cart
                }
        case actionTypes.DELETE_CART_ITEM:
                let cartdel = state.cart.filter(item => item.id !== action.item.id);
                return {
                    ...state,
                    cart:cartdel
                }
            default:
                return state
    }
}

export default reducer