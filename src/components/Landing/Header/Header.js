import React, { Component } from 'react';
import "./Header.scss"
import TextLoop from "react-text-loop"
class Header extends Component {

    render() {

        return (
            <div className='Header'>
               <div className='image'>
                   <div className='overlay'></div>
               </div>
                <div className='info'>
                    <h1><span data-aos="fade-down" data-aos-delay="500">Giving You access to a range of first class</span><span data-aos="fade-up" data-aos-delay="1200"> healthy cuisines</span></h1>
                    <p>Lorem drrv jkevt kev ke tkber kibev bikev ikbevibev kbeiev kbev evb evikbetevugev vekever cegiw erwgi weigfe i sisf i sigsf igs  evfe Lorem gk crf </p>
                    <button>GET STARTED</button>
                    <TextLoop children={["Trade faster", "Increase sales", "Stock winners", "Price perfectly"]} />
                </div>
            </div>
        )
    }
}

export default Header