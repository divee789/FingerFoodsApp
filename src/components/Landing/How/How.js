import React from "react"
import  "./How.scss"
import plate from '../../../assets/images/plateA.png'
import plateB from '../../../assets/images/plateB.png'
import order from '../../../assets/images/order.png'
import shop from '../../../assets/images/shop.png'
import deliver from '../../../assets/images/deliver.png'




const How = (props) => (
    <div className='app'>
    <img className='abs' data-aos="fade-up"
    data-aos-duration="3000" src={plate}/>
    <img className='abs2' data-aos="fade-up"
    data-aos-duration="3000" src={plateB}/>
        <h2>How it works...</h2>
        <div className="column">
            <div className="row">
                <div>
                    <h3>Get Shopping</h3>
                    <p>Duis sed viverra massa. Donec sodales metus at mollis mattis. Quisque viverra dolor nulla, ut sodales lacus feugiat in. Nunc nec bibendum nulla,. Fusce ut volutpat libero. Integer vel lacus id lorem pulvinar aliquet sed at enim. Sed nec mauris placerat, condimentum leo ac, varius nibh</p>
                </div>
                <div>
                    <img data-aos="fade-right"
                    data-aos-duration="3000" data-aos-delay='5000' src={shop} alt='Shopping'/>
                </div>
            </div>
            <div className="row">
                <div>
                    <img src={order} alt="Order"/>
                </div>
                <div>
                    <h3>Make an Order</h3>
                    <p>Duis sed viverra massa. Donec sodales metus at mollis mattis. Quisque viverra dolor nulla, ut sodales lacus feugiat in. Nunc nec bibendum nulla,. Fusce ut volutpat libero. Integer vel lacus id lorem pulvinar aliquet sed at enim. Sed nec mauris placerat, condimentum leo ac, varius nibh</p>
                </div>
            </div>
            <div className="row">
            <div>
                <h3>Specify a delivery Location</h3>
                <p>Duis sed viverra massa. Donec sodales metus at mollis mattis. Quisque viverra dolor nulla, ut sodales lacus feugiat in. Nunc nec bibendum nulla,. Fusce ut volutpat libero. Integer vel lacus id lorem pulvinar aliquet sed at enim. Sed nec mauris placerat, condimentum leo ac, varius nibh</p>
            </div>
            <div>
            <img src={deliver} alt='Delivery'/>
        </div>
        </div>
        </div>
    </div>
)


export default How