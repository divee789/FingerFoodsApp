import React from 'react'
import Logo from '../../Logo/Logo'
import './Footer.scss'

const Footer = (props) => (
    <div className="footer_app">
        <div>
            <div>
                <Logo/>
                <p>13,Bliss Avenue,Diamonds City,</p>
                <p>Elysium,Utopia.</p>
            </div>
            <div>
                <h3>Copyright Information</h3>
                <p>Sed ac dui vel neque pellentesque porttitor eget a lacus. Sed non quam elementum, eleifend ligula imperdiet, semper purus. Praesent efficitur bibendum ex, eget venenatis nisl. Nunc blandit at leo eget fermentum.</p>
                <p>Sed ac dui vel neque pellentesque porttitor eget a lacus. Sed non quam elementum, eleifend ligula imperdiet, semper purus. Praesent efficitur bibendum ex, eget venenatis nisl. Nunc blandit at leo eget fermentum.</p>
            </div>
        </div>
    </div>
)

export default Footer