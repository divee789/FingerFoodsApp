import React from 'react'
import './Start.scss'
import plateC from '../../../assets/images/plateC.png'



const start = ( props ) => (
    <div>
        <h1>Convinced Already?</h1>
        <div className='start_app'>
            <div className="start_info">
                <p>Great!! Let's Get You Started</p>
                <p><button>PLACE ORDER</button></p>
            </div>
            <div>
                <img src={plateC} alt="Dish" />
            </div>
        </div>
    </div>
)


export default start