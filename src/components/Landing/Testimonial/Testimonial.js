import React from "react"
import './Testimonial.scss'
const Testimonial = (props) => (
    <div className="app-testimonial">
        <h2>Here is proof...</h2>

        <div className='testimonials'>
            <div>
                <h3>Timely Delivery</h3>
                <hr />
                <p>Nullam laoreet dapibus odio, a congue mauris euismod in. Aliquam erat volutpat. Duis ante massa, ullamcorper et ipsum eget, rutrum posuere felis. Fusce ullamcorper mollis sem, sed commodo arcu suscipit eu. In vitae porttitor nisl, sed</p>
                <hr />
                <p><img src="https://www.picmonkey.com/blog/wp-content/uploads/2016/11/1-intro-photo-final.jpg" alt="PROFILE"/> <span>Divine Daniels</span></p>
            </div>

            <div>
                <h3>Delicious Foods</h3>
                <hr />
                <p>Nullam laoreet dapibus odio, a congue mauris euismod in. Aliquam erat volutpat. Duis ante massa, ullamcorper et ipsum eget, rutrum posuere felis. Fusce ullamcorper mollis sem, sed commodo arcu suscipit eu. In vitae porttitor nisl, sed</p>
                <hr />
                <p><img src='https://blackstudies.missouri.edu/sites/default/files/people-ing/dd5d981e-83d2-43b5-9663-8ca246c89f91.jpeg' alt='Profile'/> <span>Collins Collins</span></p>
            </div>
            <div>
                <h3>Beautiful Arrangements</h3>
                <hr />
                <p>Nullam laoreet dapibus odio, a congue mauris euismod in. Aliquam erat volutpat. Duis ante massa, ullamcorper et ipsum eget, rutrum posuere felis. Fusce ullamcorper mollis sem, sed commodo arcu suscipit eu. In vitae porttitor nisl, sed</p>
                <hr />
                <p><img src='https://ichef.bbci.co.uk/news/624/cpsprodpb/F303/production/_103611226_28429185_1867240340234530_5800880308555350016_n.jpg' alt='Profile'/> <span>Daniella Omorachi</span></p>
            </div>
        </div>
    </div>
)

export default Testimonial