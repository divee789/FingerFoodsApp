import React,{Component} from 'react'
import Aux from "../../../hoc/Auxilliary/Auxillary"
import "./Info.scss"

class Info extends Component{
  

     render(){
         return (
             <Aux>
             <h1>Why Us ?</h1>
            <div className="Info">
                <div className="text" data-aos="fade-right" data-aos-delay="1000">
                    <div className="text-body read">
                        <h3>Healthy and Hygienic</h3>
                        <p>Etiam id augue nisi. Donec turpis ante, efficitur sed dolor ut, ornare maximus velit. Praesent ultrices ultrices cursus. Phasellus sapien metus, consequat sit amet posuere ac, bibendum sed augue. Etiam ultricies lacus arcu, vel mollis libero dapibus id. Vivamus condimentum,</p>
                    </div>
                    <div className="text-body">
                        <h3>Delicious and Tasty</h3>
                        <p>
                            Quisque eget diam at libero blandit eleifend eu et nulla. Duis malesuada ut quam ut gravida. Cras aliquam laoreet ante, sit amet mattis lectus tincidunt ut. Etiam et lacus euismod, vehicula tellus at, dignissim diam. Aliquam eu commodo velit, et bibendum enim. Praesent tempus,
                     </p>
                    </div>
                    <div className="text-body">
                        <h3>Affordable and Elegant</h3>
                        <p>
                            Sed id arcu eleifend, semper mauris vel, varius nisi. Sed dignissim sagittis nisi sed aliquet. Donec nec nulla et leo viverra volutpat. Sed neque nunc, vulputate sed faucibus eu, consequat ut turpis. Morbi nisi mi, convallis id condimentum sit amet, feugiat ut felis. Sed nisl tortor,
                     </p>
                    </div>
                    <div className="text-body">
                        <h3>Delivered Right to Your Doorsteps</h3>
                        <p>
                            Fusce tincidunt mauris sit amet erat venenatis, scelerisque mollis eros dictum. Pellentesque porttitor ut lorem in varius. Curabitur in elementum leo. Duis sit amet sollicitudin leo. Nunc libero lacus, ornare in nisi nec, ultrices hendrerit urna. Sed vel lectus quis quam tempo
                   </p>
                    </div>
                </div>
                <div className="Image" data-aos="fade-down" data-aos-delay="500">
                  <div className="overlay"></div>
                </div>
            </div>
            </Aux>
        )
     }

}


export default Info