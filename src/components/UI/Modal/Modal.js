import React from 'react'

import Aux from "../../../hoc/Auxilliary/Auxillary"
import Backdrop from "../Backdrop/Backdrop"
import './Modal.scss'

const Modal = (props)=>(
    <Aux>
    <Backdrop show={props.show} clicked={props.closeModal}/>
    <div 
    className='modal_app'
     style={{
         display:props.show?'block':'none',
         transform:props.show?'translateX(0)':'translateX(-100vh)',
         opacity: props.show ? "1": "0"
     }}    >
       {props.children}
    </div>
    </Aux>
)


export default Modal