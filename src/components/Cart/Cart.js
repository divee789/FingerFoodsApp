import React, { Component } from 'react'
import { connect } from 'react-redux'

import Aux from '../../hoc/Auxilliary/Auxillary'
import Backdrop from '../UI/Backdrop/Backdrop'
import img from "../../assets/images/cart.png"
import empty from "../../assets/images/empty.png"
import './Cart.scss'
import * as actionTypes from "../../store/actions/index";
class Cart extends Component {

    render() {
        let attachedClasses = ['cart', 'Hide'];
        if (this.props.open) {
            attachedClasses = ['cart', 'Show']
        }
        let info
        let text
        if (this.props.cart === undefined || this.props.cart.length == 0) {
            info = <div className='cart_empty'>
                <div><img src={empty} /></div>
                <div className="info_text">Your Cart Is Empty</div>
                <div className="continue">Continue Shopping?</div>
            </div>
            text="NOT REALLY"
        } else {
            info = this.props.cart.map(item => {
                return (<Aux key={`${item.id}`}>

                    <div className="cart_item" >
                        <div>ID: {item.id}</div>
                        <div>{item.name}</div>
                        <div>NGN {item.price}</div>
                        <div onClick={()=>this.props.onDeleteCartItem(item)}>Delete</div>
                    </div>
                </Aux>)
            })
            text="CHECKOUT"
        }
        return (
            <Aux>
                <Backdrop show={this.props.open} clicked={this.props.close} />
                <div className={attachedClasses.join(' ')}>
                    <h2>My  Cart</h2><img src={img} />
                    <div className='cart_items'>
                        {info}
                    </div>
                    <button>{text}</button>
                </div>
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart.cart
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onDeleteCartItem: (item) => dispatch(actionTypes.deleteCartItem(item))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cart)