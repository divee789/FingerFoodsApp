import React from 'react';
import Logo from "../../Logo/Logo"
import NavigationItems from "../NavigationItems/NavigationItems"
import Backdrop from '../../UI/Backdrop/Backdrop'
import Aux from "../../../hoc/Auxilliary/Auxillary"
import "./Sidenav.scss"
const Sidenav = (props) => {
   let attachedClasses = ['sidenav','Close'];
   if(props.open){
     attachedClasses = ['sidenav','Open']
   }

    return (
        <Aux>
            <Backdrop show={props.open} clicked={props.closed}/>
            <div className={attachedClasses.join(' ')}>
                <Logo align="center" />
                <nav>
                    <NavigationItems />
                </nav>
            </div>
        </Aux>
    )
}

export default Sidenav