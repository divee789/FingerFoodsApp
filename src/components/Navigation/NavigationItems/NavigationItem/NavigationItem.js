import React from 'react';
import "./NavigationItem.scss"
import { NavLink } from 'react-router-dom'

const NavigationItem = (props)=>(
     <li className="item"><NavLink to={props.link} activeClassName='active' exact>{props.children}</NavLink></li>
)

export default NavigationItem