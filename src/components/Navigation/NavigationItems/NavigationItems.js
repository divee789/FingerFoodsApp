 import React from 'react';
 import "./NavigationItems.scss"
 import NavigationItem from "./NavigationItem/NavigationItem"

 const NavigationItems=(props)=>(
     <ul className="items">
       <NavigationItem link="/">About</NavigationItem>
       <NavigationItem link="/products">Products</NavigationItem>
       <NavigationItem link="/premium">Premium</NavigationItem>
       <NavigationItem link="/register">Sign Up</NavigationItem>
       <NavigationItem link="/logIn">Log In</NavigationItem>
     </ul>
 )


 export default NavigationItems