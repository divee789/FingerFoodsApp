import React, { Component } from 'react';
import Logo from "../../Logo/Logo"
import NavigationItems from "../NavigationItems/NavigationItems"
import Toggle from "../Sidenav/Toggle/Toggle"
import "./NavBar.scss"
class NavBar extends Component{
    render(){
        return(
          <header className='NavBar'>
            <Toggle clicked={this.props.toggleClicked} />
              <Logo/>
              <nav className="desktop">
              <NavigationItems/>
              </nav>
          </header>
        )
    }
}

export default NavBar