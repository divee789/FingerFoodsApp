import React from "react"
import './Logo.scss'

const Logo = (props)=>(
     <div>
         <div className='Logo' style={{textAlign:props.align}}>aLPHA</div>
     </div>
)

export default Logo