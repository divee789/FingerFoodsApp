import React, { Component } from 'react'
import Aux from '../../hoc/Auxilliary/Auxillary'
import NavBar from '../Navigation/NavBar/NavBar'
import './Layout.scss'
import Sidenav from "../Navigation/Sidenav/Sidenav"
class Layout extends Component {
    state = {
        showSideNav:false
    }

    sideNavHandler=()=>{
        this.setState({showSideNav:false})
    }
    toggleHandler = ()=>{
        this.setState((prevState)=>{
            return {showSideNav:!prevState.showSideNav}
        })
    }
    render() {
        return (
            <Aux>
                <NavBar toggleClicked={this.toggleHandler}/>
                <Sidenav open={this.state.showSideNav} closed = {this.sideNavHandler}/>
                <main className='Content'>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}

export default Layout