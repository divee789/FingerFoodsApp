import React from 'react'
import './Products.scss'
const Products = (props) => (  
            <div className="card">
                <img src='http://excelfoodmart.com.au/wp-content/uploads/2018/06/846656010297.jpg' alt='food Image' />
                <div>
                <p>Name: {props.name}</p>
                <p>Recipe: {props.recipe}</p>
                <p>Price: NGN {props.price}</p>
               <div>
               <button onClick={props.click}>ORDER NOW</button>
               <button onClick={props.addToCart}>ADD TO CART</button>
               </div>
                </div>
            </div>
)

export default Products