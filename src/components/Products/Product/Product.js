import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actionTypes from '../../../store/actions'

import Aux from "../../../hoc/Auxilliary/Auxillary"

import "./Product.scss"
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import 'animate.css';


class Product extends Component { 
    render() {
        const orderItem = this.props.ord.product.product
        return (
            <Aux>
                <div className="product_component">
                    <div className='product_image'><img src='http://excelfoodmart.com.au/wp-content/uploads/2018/06/846656010297.jpg' alt='food Image' /></div>
                    <div className='product_details'>
                        <h1>Your Order</h1>
                        <p>Name: {this.props.product.name} </p>
                        <p>Recipe: {this.props.product.recipe}</p>
                        <p>Price:NGN {this.props.product.price}</p>
                        <p><button onClick={async () => {
                            await console.log("hi")
                            await this.props.onOrderProduct(orderItem)
                            store.addNotification({
                                title: 'Order',
                                message: 'Your Order Is ready,Pls proceed to the orders page',
                                type: 'success',                         // 'default', 'success', 'info', 'warning'
                                container: 'top-center',                // where to position the notifications
                                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                                dismiss: {
                                  duration: 5000 ,
                                  onScreen:true,
                                  pauseOnHover:true
                                }
                              })
                            console.log(this.props.ord.product.product)
                        }}>PLACE ORDER</button><button onClick={this.props.closeModal}>CANCEL ORDER</button></p>
                    </div>
                </div>

            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        ord: state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onOrderProduct: (orderItem) => dispatch(actionTypes.orderProduct(orderItem))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Product)