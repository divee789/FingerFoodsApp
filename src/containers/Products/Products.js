import React, { Component } from 'react'
import { connect } from 'react-redux'
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import 'animate.css';


import Aux from '../../hoc/Auxilliary/Auxillary'
import './Products.scss'
import Produce from '../../components/Products/Products'
import Product from '../../components/Products/Product/Product'
import Footer from '../../components/Landing/Footer/Footer'
import Modal from '../../components/UI/Modal/Modal'
import Cart from '../../components/Cart/Cart'
import * as actionTypes from '../../store/actions/index'



class Products extends Component {
    state = {
        purchasing: false,
        addCart: false
    }

    cartHandler = () => {
        this.setState({
            ...this.state,
            addCart: true
        })
    }
    closeCartHandler = () => {
        this.setState({ addCart: false })
    }

    orderStartHandler = async (product) => {
        this.props.onSeeProduct(product)
        this.setState({
            ...this.state,
            purchasing: true
        })
    }

    orderCancelHandler = () => {
        this.setState({
            ...this.state,
            purchasing: false
        })
    }
    render() {

        return (
            <Aux>
                <button onClick={this.cartHandler} className='cartBtn'>MY CART</button>
                <Cart open={this.state.addCart} close={this.closeCartHandler} />
                <Modal show={this.state.purchasing} closeModal={this.orderCancelHandler}><Product product={this.props.product} closeModal={this.orderCancelHandler} /></Modal>
                <div className='products'>
                    <div className='overlay'>
                        <h1>Available Products</h1>
                        <div className='product_list'>
                            {this.props.prod.map(product => {
                                return <Produce key={product.id} name={product.name} recipe={product.recipe} price={product.price} click={() => this.orderStartHandler(product)}
                                    addToCart={async () => {
                                        await console.log("hi cart")
                                        await this.props.onAddToCart(product)
                                        store.addNotification({
                                            title: 'MY CART',
                                            message: 'SUCCESSFULLY ADDED',
                                            type: 'success',                         // 'default', 'success', 'info', 'warning'
                                            container: 'top-center',                // where to position the notifications
                                            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                                            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                                            dismiss: {
                                                duration: 5000,
                                                onScreen: true,
                                                pauseOnHover: true
                                            }
                                        })
                                        console.log(this.props.stat)
                                    }} />
                            })}</div>
                    </div>
                    <Footer />
                </div>
            </Aux>
        )
    }

}

const mapStateToProps = state => {
    return {
        prod: state.product.products,
        product: state.product.product,
        stat: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSeeProduct: (product) => dispatch(actionTypes.seeProduct(product)),
        onAddToCart: (cartItem) => dispatch(actionTypes.addToCart(cartItem))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)





