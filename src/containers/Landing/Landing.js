import React, { Component } from 'react';
import Aux from '../../hoc/Auxilliary/Auxillary'
import Header from '../../components/Landing/Header/Header'
import Info from "../../components/Landing/Info/Info"
import How from "../../components/Landing/How/How"
import Testimonial from '../../components/Landing/Testimonial/Testimonial'
import Start from '../../components/Landing/Start/Start'
import Footer from '../../components/Landing/Footer/Footer'
class Landing extends Component {
    render() {


        return (
            <Aux>
                <Header />
                <How />
                <Info />
                <Testimonial />
                <Start />
                <Footer/>
            </Aux>
        )
    }
}

export default Landing